import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, FlatList, ImageBackground} from 'react-native';
import Inputan from '../components/Inputan';
import RadioButton from '../components/RadioButton';
import {ActionPrabayar, ActionInquiryRequest} from './Redux/action';
import {connect} from 'react-redux';
import {
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
} from 'react-native-gesture-handler';
import Button from '../components/Button';
import Gap from '../components/Gap';
import axios from 'axios';
import {backgroundinquiry} from '../assets';
import Loading from '../components/Loading';
import {navigate} from '../utils/Nav';

const Home = props => {
  const [nav, setNav] = useState(false);

  const [kode, setKode] = useState('');
  const [pra, setPra] = useState('');
  const [nomorpilihan, setNomorPilihan] = useState('');
  const [nomorpra, setNomorPra] = useState('');

  const [nomor, setNomor] = useState('');

  // const =()=>{
  //   launchIma
  // }
  // const nomorpilihan = '';

  useEffect(() => {
    props.ActionPrabayar();
  }, []);

  return (
    <>
      {props.isLoading ? (
        <Loading />
      ) : (
        <ImageBackground source={backgroundinquiry} style={styles.background}>
          <Text style={styles.tulisan}>Pilih pembayaran : </Text>
          <View style={styles.back}>
            <RadioButton
              clickPrabayar={() => {
                // setNav(false);
                setKode('0');
                setNomorPra('PREPAID20K');
              }}
              clickPasca={() => {
                setKode('1');
                setNomorPilihan('PLN-POSTPAID');
              }}
            />
            <Inputan onChangeText={text => setNomor(text)} value={nomor} />
            {/* <ScrollView showsVerticalScrollIndicator={false}> */}
            {kode === '1' ? (
              <Text style={styles.pasca}>-----</Text>
            ) : kode === '0' ? (
              <FlatList
                showsVerticalScrollIndicator={false}
                data={props.listToken}
                renderItem={({item, index}) => {
                  return (
                    <TouchableOpacity
                      key={index}
                      style={styles.token}
                      onPress={() => {
                        // console.log(item.code, 'ini item');
                        // const nomorPilihan = item.code;
                        // console.log(nomorPilihan, 'ini milih');
                        setNomorPilihan(item.code);

                        // const nomorpilihan = item;
                        // const kodelagi = props.listToken.code;
                        // console.log(kodelagi, 'ini kode yak');
                      }}>
                      <View style={styles.pilihtoken}>
                        <Text style={styles.tulisansatu}>{item.item}</Text>
                        <Text style={styles.tulisan}>{item.code}</Text>
                        <Text style={styles.tulisan}>Rp. {item.price}</Text>
                      </View>
                    </TouchableOpacity>
                  );
                }}
              />
            ) : null}

            <Gap height={20} />
            <Button
              title="Submit"
              // click={() => {
              //   props.navigation.navigate('Inquiry');
              // }}
              onPress={() => props.ActionInquiryRequest(nomorpilihan, nomor)}
              // onPress={() => console.log(nomorpilihan, 'ini pilih')}
            />
            <Gap height={30} />
          </View>
        </ImageBackground>
      )}
    </>
  );
};

const mapStateToProps = state => ({
  listToken: state.PrabayarReducer.nomor,
  isLoading: state.GlobalReducer.isLoading,
});

const mapDispatchToProps = {
  ActionPrabayar,
  ActionInquiryRequest,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);

const styles = StyleSheet.create({
  back: {
    flex: 1,
    // backgroundColor: '#ecf0f1',
  },
  pilihtoken: {
    borderWidth: 1,
    borderRadius: 10,
    marginVertical: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  pasca: {
    justifyContent: 'center',
    alignSelf: 'center',
  },
  token: {
    paddingHorizontal: 30,
  },
  tulisansatu: {
    fontSize: 25,
    fontWeight: 'bold',
  },
  background: {
    flex: 1,
  },
  tulisandua: {
    fontSize: 20,
  },
  tulisantiga: {
    fontSize: 15,
  },
  tulisan: {
    fontSize: 15,
    fontWeight: 'bold',
    justifyContent: 'center',
    alignSelf: 'center',
    paddingTop: 30,
  },
});
