const InitialState = {
  nomor: '',
  kodeproduk: '',
  nomorpelanggan: '',
  respon: {},
};

export const PrabayarReducer = (state = InitialState, action) => {
  switch (action.type) {
    case 'SET_PRABAYAR':
      return {
        ...state,
        nomor: action.nomor,
      };
    case 'SET_INQUIRY':
      return {
        ...state,
        kodeproduk: action.kode,
        nomorpelanggan: action.nomor,
        respon: action.respon,
      };

    default:
      return state;
  }
};
