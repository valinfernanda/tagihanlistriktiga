import React, {useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {CheckBox} from 'react-native-elements';

const RadioButton = ({clickPrabayar, clickPasca}) => {
  const [check, setcheck] = useState(false);

  return (
    <View style={styles.check}>
      <CheckBox
        title="Prabayar"
        checked={check}
        checkedIcon="dot-circle-o"
        uncheckedIcon="circle-o"
        containerStyle={styles.checkboxStyle}
        onPress={() => {
          setcheck(!check);
          clickPrabayar();
        }}
      />
      <CheckBox
        title="Pascabayar"
        checked={check}
        checkedIcon="dot-circle-o"
        uncheckedIcon="circle-o"
        containerStyle={styles.checkboxStyle}
        onPress={() => {
          setcheck(!check);
          clickPasca();
        }}
      />
    </View>
  );
};

export default RadioButton;

const styles = StyleSheet.create({
  check: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  checkboxStyle: {
    borderRadius: 10,
    // backgroundColor: '#2980b9',
    backgroundColor: '#C7C9CA',
    borderWidth: 2,
    borderColor: '#6E767F',

    color: 'white',
  },
});
